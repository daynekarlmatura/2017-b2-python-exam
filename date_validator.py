#!/usr/bin/env python
import datetime

def date_validator(date):
	correctDate = None
	month, day, year = date.split('/')
	month = int(month)
	day = int(day)
	year = int(year)
	
	try:
	    newDate = datetime.datetime(year,month,day)
	    correctDate = True
	except ValueError:
	    correctDate = False
	return correctDate

inp = raw_input('Enter Date (MM/DD/YY): ')
isvalid = date_validator(inp)

if isvalid == True:
	print "Date is Valid"
else:
	print "Date is Invalid"