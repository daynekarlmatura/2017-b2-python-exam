#!/usr/bin/env python

def compute_magic_number(name):
	output = []
	num = 0
	finalnum = 0
	for character in name:
	    number = ord(character) - 96
	    output.append(number)

	for i in range(len(output)):
		num += int(output[i])

	if num > 9:
		finalnum = sum_digits(num)
		return finalnum
	else:
		return num

def sum_digits(n):
    s = 0

    while n:
        s += n % 10
        n //= 10
    return s


inp = str(raw_input('Enter your name: '))
inp = inp.lower()

magicnum = compute_magic_number(inp)
print "Your magic number is: "+str(magicnum)