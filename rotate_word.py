#!/usr/bin/env python

def rotate_word(integer, stri):
	rotated = ''
	asc = 0
	temp = 0

	for i in range(len(stri)):
		asc = ord(stri[i])
		temp = asc + integer
		rotated += chr(temp)


	return rotated

inp = raw_input('Enter String: ')
inte = int(raw_input('Enter Number Basis for Rotation: '))
new_exp = rotate_word(inte, inp)
print "New Encrypted String: " + new_exp