#!/usr/bin/env python

def replace_x(stri):
	
	new_string = ''

	for i in range(len(stri)):
		if i == 0 or i == 1:
			if stri[i] != 'x':
				new_string += stri[i]
		else:
			new_string += stri[i]

	return new_string

inp = raw_input('Enter String: ')
new_str = replace_x(inp)
print "New String: " + new_str