#!/usr/bin/env python
name = ''
math = []
science = []
english = []
avema = 0
avesc = 0
aveen = 0
dic = {}
sred = {}

def input_grade():

	name = str(raw_input('Enter Name of Student: '))
	
	for i in range(3):
		if i == 0:
			print 'PRELIMS'
			math.append(raw_input('Enter Grade in Math: '))
			science.append(raw_input('Enter Grade in Science: '))
			english.append(raw_input('Enter Grade in English: '))
		elif i == 1:
			print 'MIDTERMS'
			math.append(raw_input('Enter Grade in Math: '))
			science.append(raw_input('Enter Grade in Science: '))
			english.append(raw_input('Enter Grade in English: '))
		elif i == 2:
			print 'FINALS'
			math.append(raw_input('Enter Grade in Math: '))
			science.append(raw_input('Enter Grade in Science: '))
			english.append(raw_input('Enter Grade in English: '))

	dic['Math'] = get_students_average(math)
	dic['Science'] = get_students_average(science)
	dic['English'] = get_students_average(english)

	sred = sorted(dic.items(), key=lambda value: value[1])
	sred.reverse()
	
	for i in range(len(sred)):
		print str(sred[i][0])+" : "+ str(sred[i][1])

def get_students_average(x):
	a=0
	for i in range(len(x)):
		a += int(x[i])
	
	a= a/3
	return a

input_grade()

