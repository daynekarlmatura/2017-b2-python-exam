#!/usr/bin/env python
import re

def builtin(expression):
    if '+' in expression:
        y = expression.split('+')
        x = int(y[0])+int(y[1])
    elif '-' in expression:
        y = expression.split('-')
        x = int(y[0])-int(y[1])
    elif '/' in expression:
        y = expression.split('/')
        x = int(y[0])/int(y[1])
    elif '*' in expression:
        y = expression.split('*')
        x = int(y[0])*int(y[1])
    return x

expression = str(raw_input("Please enter basic mathemtical expression: "))
expressionregex = re.sub(r"(\d+)([a-z])", r"\1*\2", expression)

solve = builtin(expressionregex)

print solve