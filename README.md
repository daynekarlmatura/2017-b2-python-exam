# PYTHON HANDS-ON EXAM #

### replace.py ###
Write a function named ```replace_x()``` that, given a string, if one or both of the first 2 chars is 'x', return the string without those 'x' chars, and otherwise return the string unchanged.

### explode.py ###
Write a function named ```explode()``` which returns an exploded version of its string parameter such that calling explode(‘Action’)for example, returns ‘AAcActActiActioAction’, explode(‘think’)for example, returns ‘tththithinthink’,and so on.

### chinese_puzzle.py ###
Write a function to solve a classic ancient Chinese puzzle:
We count 35 heads and 94 legs among the chickens and rabbits in a farm. How many rabbits and how many chickens do we have?
Use ```heads``` and ```legs``` as parameters for heads and legs, respectively, while use ```rabbits``` and ```chickens``` as parameters for the number of rabbits and chickens, respectively.

### builtin.py ###
Please write a program which accepts basic mathematic expression from console and print the evaluation result.
Example:
If the following string is given as input to the program:
```35+3```
Then, the output of the program should be:
```38```

### list.py ###
By using list comprehension, please write a program to print the list after removing the 0th, 2nd, 4th, 6th, etc... numbers in any given list.
e.g.) ```[12,24,35,70,88,120,155]``` will become ```[24,70,120]```

### numerology.py ###
Numerologists claim to be able to determine a person's character traits based on the "numeric value" of a name, which, as they call, "Magic Number". The value of a name is determined by summing up the values of the letters of the name where "a" is 1, "b" is 2, etc., up to "z" being 26. If the sum is a two-digit number, except 11 and 13, the digits of these numbers are then summed up, until we form a one-digit number, an 11, or a 13.
For example, the name "Zelle" would have the value of 26 + 5 + 12 + 12 + 5 = 60. Since 60 is two-digit, we then add the digits together: 6 + 0 = 6. Thus, her Magic Number is 6.
Write a function named ```compute_magic_number``` that calculates the magic number of a name provided as a parameter.

### rotate_word.py ###
ROT13 is a weak form of encryption that involves “rotating” each letter in a word by 13 places. To rotate a letter means to shift it through the alphabet, wrapping around to the  beginning if necessary, so ’A’ shifted by 3 is ’D’ and ’Z’ shifted by 1 is ’A’.
Write a function called ```rotate_word``` that takes a string and an integer as parameters, and that returns a new string that contains the letters from the original string “rotated” by the given amount.
For example, “cheer” rotated by 7 is “jolly” and “melon” rotated by -10 is “cubed”.
*hint:  Python has built-in functions chr() and ord() that converts an int value to equivalent ASCII character and vice-versa*

### employee.py ###
An employee of a certain company is eligible to be a Team Leader if they are at least 30 years old, but not older than 65, and have been with the company for at least 5 years.
Write a class named ```Employee``` that has attributes ```first_name```, ```last_name```, ```birthday```, and ```date_of_employment```, and a method that returns a boolean data representing his/her eligibility for Team Leader.
Also, provide a function that would return a list of names eligible as Team Leader *(in the format **Last Name, First Name**)*.

### date_validator.py ###
Write a function named date_validator that accepts a date (in the form of string), then checks if it is in the format MM/DD/YYYY. If it is, return a boolean data representing if such date is valid (For example 05/24/1962 is valid, but 09/31/2000 is not, since September only has 30 days). If not, return a string that tells the user to input a string in the valid format.

### grades.py ###
Write two functions named ```input_grade``` and ```get_students_average```.
The first function would be able to ask the user to enter a student’s name and its grades on the following subjects: Mathematics, Science, and English, then saves this data in a file.
The second function will get all data inside the file, and display all students with their corresponding grade average, sorted out by their average (highest average is displayed first).
